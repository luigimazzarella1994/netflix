//
//  ViewController.swift
//  Netflix
//
//  Created by Luigi Mazzaerella on 04/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var library: [Film] = []
    
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var category1: UILabel!

    
    
    func updateLibrary(){
        library.append(Film(title: "Harry Potter", image: UIImage(named: "harryPotter")!, categories: ["Fantasy"]))
        library.append(Film(title: "Spiderman", image: UIImage(named: "spiderman")!, categories: ["Supereroi", "Fantasy"]))
        library.append(Film(title: "La Casa Di Carta", image: UIImage(named: "casaDiCarta")!, categories: ["Action","Drammaric"]))
    }
    
    override func viewDidLoad() {
        updateLibrary()
        firstImage.image = library[1].image
        title1.text  = library[1].title
        category1.text = library[1].categories.joined(separator: "•")
       
       
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
  
    
    @IBAction func next(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: "FilmTableViewController") as! FilmTableViewController
        vc.self.filmContainer = library
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

