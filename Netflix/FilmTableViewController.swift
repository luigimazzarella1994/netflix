//
//  FilmTableViewController.swift
//  Netflix
//
//  Created by Luigi Mazzaerella on 04/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class filmCell: UITableViewCell {
    @IBOutlet weak var insideCollection: UICollectionView!
    @IBOutlet weak var categoryFilm: UILabel!
    
}
class InsideCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var coverImage: UIImageView!
 
}

class FilmTableViewController: UITableViewController,UICollectionViewDataSource, UICollectionViewDelegate {
      
    var filmContainer: [Film] = []
    var category: [String] = []
    
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filmContainer.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serie", for: indexPath) as! InsideCollectionViewCell
           cell.coverImage.image = filmContainer[indexPath.row].image
           // Configure the cell...
           
           return cell
       }
    
    func numberOfCategory(){
        
        for film in filmContainer {
            for category in film.categories {
                <#code#>
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLibrary()
        
        print("film \(filmContainer)")

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "film", for: indexPath) as! filmCell

        // Configure the cell...
        
        
        return cell
    }
    
    func updateLibrary(){
        filmContainer.append(Film(title: "Harry Potter", image: UIImage(named: "harryPotter")!, categories: ["Fantasy"]))
        filmContainer.append(Film(title: "Spiderman", image: UIImage(named: "spiderman")!, categories: ["Supereroi", "Fantasy"]))
        filmContainer.append(Film(title: "La Casa Di Carta", image: UIImage(named: "casaDiCarta")!, categories: ["Action","Drammaric"]))
        filmContainer.append(Film(title: "Final Space", image: UIImage(named: "finalSpace")!, categories: ["cartoon","comedy"]))
    }
  

}

extension filmCell
{
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDelegate  & UICollectionViewDataSource>
        (_ dataSourceDelegate: D, forRow row : Int)
    {
        insideCollection.delegate = dataSourceDelegate
        insideCollection.dataSource = dataSourceDelegate
        
        insideCollection.reloadData()
    }
}
