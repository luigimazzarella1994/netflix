//
//  Film.swift
//  Netflix
//
//  Created by Luigi Mazzaerella on 04/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import Foundation
import UIKit

class Film {
    var title: String = ""
    var image: UIImage! = nil
    var categories : [String] = []
    
    init(title: String, image: UIImage, categories: [String]) {
        self.title = title
        self.image = image
        self.categories = categories
    }
}
